# TP2 : Serveur Web

# Sommaire

* [I. Base de données](#i-base-de-données)
* [II. Serveur Web](#ii-serveur-web)
* [III. Reverse Proxy](#iii-reverse-proxy)
* [IV. Un peu de sécu](#iv-un-peu-de-sécu)
    * [1. fail2ban](#1-fail2ban)
    * [2. HTTPS](#2-https)
        * [A. Génération du certificat et de la clé privée associée](#a-génération-du-certificat-et-de-la-clé-privée-associée)
        * [B. Configurer le reverse proxy](#b-configurer-le-reverse-proxy)
    * [3. Monitoring](#3-monitoring)
        * [A. Installation de Netdata](#a-installation-de-netdata)
        * [B. Alerting](#b-alerting)

<!-- vim-markdown-toc -->

# I. Base de données

**Énoncé :**

Installer le paquet `mariadb-server`, puis démarrer le service associé.

Il faudra créer un utilisateur dans l'instance de base de données, ainsi qu'une base dédiée. Référez-vous à des docs/articles que vous pourrez trouver sur internet concernant la préparation d'une base de données pour une utilisation avec Wordpress. Les étapes :
* se connecter à la base
* créer une base
* créer un utilisateur
* attribuer les droits sur la base de données à l'utilisateur 
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base

Pour connaître le port utilisé par la base de données, on peut uiliser la commande `ss`. Une fois la base de données lancée, on peut par exemple la commande suivante pour déterminer le port utilisé par la base :
```bash
$ sudo ss -alnpt
```

Pour tester votre base de données :
```bash
$ mysql -u <USER> -p -h <IP_BASE_DE_DONNEES> -P <PORT_BASE_DE_DONNEES>

# Exemple de commande SQL à taper pour vérifier que la base fonctionne
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
+--------------------+
2 rows in set (0.00 sec)
```
---
**Solution :**

* Installer le paquet mariadb-server : 
```bash
[steve@db ~]$sudo yum install mariadb-server -y
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.ircam.fr
 * extras: centos.crazyfrogs.org
 * updates: centos.crazyfrogs.org
base                                                  | 3.6 kB  00:00:00
[…]
  perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7  perl-DBD-MySQL.x86_64 0:4.023-6.el7
  perl-DBI.x86_64 0:1.627-4.el7                perl-Data-Dumper.x86_64 0:2.145-3.el7
  perl-IO-Compress.noarch 0:2.061-2.el7        perl-Net-Daemon.noarch 0:0.48-5.el7
  perl-PlRPC.noarch 0:0.2020-14.el7
  Complete!
```

* Démarrer service MariaDB : 
```bash
[steve@db ~]$ sudo systemctl start mariadb.service
[steve@db ~]$ sudo systemctl enable mariadb.service
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
```

* Se connecter à la base : 
```bash
[steve@db ~]$ mysql --user root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 2
Server version: 5.5.68-MariaDB MariaDB Server
Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
MariaDB [(none)]>
```

* Créer une base : 
```bash
MariaDB [(none)]> CREATE DATABASE db_web;
Query OK, 1 row affected (0.00 sec)
```


* Créer un utilisateur
```bash
MariaDB [(none)]> CREATE USER 'dbweb'@'10.99.99.12' IDENTIFIED BY '******';
Query OK, 0 rows affected (0.00 sec)
```
* Vérification de la création :
```bash
MariaDB [(none)]> select user,host from mysql.user;
+-------+-------------+
| user  | host        |
+-------+-------------+
| dbweb | 10.99.99.12 |
| root  | 127.0.0.1   |
| root  | ::1         |
|       | db.tp2.lab  |
| root  | db.tp2.lab  |
|       | localhost   |
| root  | localhost   |
+-------+-------------+
7 rows in set (0.00 sec)
```

* Attribuer les droits sur la base de données à l'utilisateur : 

```bash
MariaDB [(none)]> GRANT all privileges on db_web.* to 'dbweb'@'10.99.99.12';
Query OK, 0 rows affected (0.00 sec)
#Vérification
MariaDB [(none)]> show grants for 'dbweb'@'10.99.99.12';
+-------------------------------------------------------------+
| Grants for dbweb@10.99.99.12                                |
+-------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'dbweb'@'10.99.99.12'                 |
| GRANT ALL PRIVILEGES ON `db_web`.* TO 'dbweb'@'10.99.99.12' |
+-------------------------------------------------------------+
2 rows in set (0.00 sec)
```

* Ouvrir un port firewall pour que d'autres machines puissent accéder à la base : 
```bash
[steve@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[steve@db ~]$ sudo firewall-cmd --reload
success
```
* Vérification de l'ouverture : 
```bash
[steve@db ~]$ sudo firewall-cmd --list-all
[...]
ports: 8888/tcp 80/tcp 3306/tcp
[...]
```
---
**Vérification :**

```bash
[steve@db ~]$ mysql -u dbweb -h 10.99.99.12 -p -P 3306
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 31
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

* UPDATE de dbweb pour une connexion depuis web.tp2.lab : 
```bash
MariaDB [(none)]> UPDATE mysql.user SET Host='10.99.99.11' WHERE Host='10.99.99.12' AND User='dbweb';
Query OK, 0 rows affected (0.00 sec)
Rows matched: 0  Changed: 0  Warnings: 0
MariaDB [(none)]> FLUSH PRIVILEGES ;
Query OK, 0 rows affected (0.00 sec)
#Vérification
MariaDB [(none)]> select user,host from mysql.user;
+-------+-------------+
| user  | host        |
+-------+-------------+
| dbweb | 10.99.99.11 |
| root  | 127.0.0.1   |
| root  | ::1         |
|       | db.tp2.lab  |
| root  | db.tp2.lab  |
|       | localhost   |
| root  | localhost   |
+-------+-------------+
7 rows in set (0.00 sec)
```

# II. Serveur Web

**Énoncé :**

Installer un serveur Apache (paquet `httpd` dans CentOS).

Télécharger Wordpress : https://wordpress.org/latest.tar.gz

Idem ici, référez-vous à un des milliers d'article/doc/tuto que vous pourrez trouver. Faites vos recherches en anglais et précisez l'OS qu'on utilise.

Les étapes :
* installer le serveur Web (`httpd`) et le langage PHP
* télécharger wordpress
* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)
* configurer Apache pour qu'il serve le dossier où se trouve Wordpress
* lancer le serveur de base de donnée, puis le serveur web
* ouvrir le port firewall sur le serveur web
* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

---

**Solution :**

* Installer un serveur Apache : 
```bash
[steve@web ~]$ sudo yum install httpd -y
Dependency Installed:
apr.x86_64 0:1.4.8-7.el7        apr-util.x86_64 0:1.5.2-6.el7   httpd-tools.x86_64 0:2.4.6-97.el7.centos
mailcap.noarch 0:2.1.41-2.el7
Complete!
```
* * Démarrage du service *httpd* :
```bash
[steve@web ~]$ sudo systemctl start httpd
[steve@web ~]$ sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
```

---
* Installer langage PHP : 

*    * Installation de dépôt additionels : 
```bash
[steve@web ~]$sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
Installed:
remi-release.noarch 0:7.9-1.el7.remi
Dependency Installed:
epel-release.noarch 0:7-11
Complete!
[steve@web ~]$sudo yum install -y yum-utils
Installed:
yum-utils.noarch 0:1.1.31-54.el7_8
Dependency Installed:
libxml2-python.x86_64 0:2.9.1-6.el7.5       python-chardet.noarch 0:2.2.1-3.el7
python-kitchen.noarch 0:1.1.1-5.el7
Complete!
```


*    * On supprime d'éventuelles vieilles versions de PHP précédemment installées : 

```bash
Loaded plugins: fastestmirror
No Match for argument: php
No Packages marked for removal
# Activation du nouveau dépôt
[steve@web ~]$ sudo yum-config-manager --enable remi-php56
Loaded plugins: fastestmirror
============================================= repo: remi-php56 =============================================
[remi-php56]
[...]
```
*    *    Installation de PHP 5.6.40 et de librairies récurrentes

```bash
[steve@web ~]$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
[...]
Installed:
  php.x86_64 0:5.6.40-24.el7.remi                    php-cli.x86_64 0:5.6.40-24.el7.remi
  php-common.x86_64 0:5.6.40-24.el7.remi             php-gd.x86_64 0:5.6.40-24.el7.remi
  php-ldap.x86_64 0:5.6.40-24.el7.remi               php-mcrypt.x86_64 0:5.6.40-24.el7.remi
  php-mysqlnd.x86_64 0:5.6.40-24.el7.remi            php-pecl-zip.x86_64 0:1.19.2-1.el7.remi.5.6

Dependency Installed:
  dejavu-fonts-common.noarch 0:2.33-6.el7           dejavu-sans-fonts.noarch 0:2.33-6.el7
  fontconfig.x86_64 0:2.13.0-4.3.el7                fontpackages-filesystem.noarch 0:1.44-8.el7
[...]
  php-pdo.x86_64 0:5.6.40-24.el7.remi               php-pecl-jsonc.x86_64 0:1.3.10-2.el7.remi.5.6
  t1lib.x86_64 0:5.1.2-14.el7

Complete!
```

* * Restart du service Apache pour qu'il prenne en compte le module PHP : 
```bash
[steve@web ~]$ sudo systemctl restart httpd
```
---
* Télécharger wordpress :

*    * Installation paquet wget : 
```bash
[steve@web ~]$ sudo yum install wget -y
[...]
Installed:
wget.x86_64 0:1.14-18.el7_6.1
Complete!
```
* * Téléchargement de Wordpress : 
```bash
[steve@web ~]$ sudo mkdir download
[steve@web ~]$ cd download/
[steve@web download]$ wget https://wordpress.org/latest.tar.gz
--2021-01-06 15:28:14--  https://wordpress.org/latest.tar.gz
Resolving wordpress.org (wordpress.org)... 198.143.164.252
Connecting to wordpress.org (wordpress.org)|198.143.164.252|:443... connected.
...]
Saving to: ‘latest.tar.gz’

100%[==================================================================>] 15,422,346   401KB/s   in 37s

2021-01-06 15:29:05 (410 KB/s) - ‘latest.tar.gz’ saved [15422346/15422346]
```
---
* Extraire l'archive wordpress dans un dossier qui pourra être servi par Apache :
```bash
[steve@web download]$ sudo tar xzvf latest.tar.gz
wordpress/
[...]
wordpress/wp-trackback.php
wordpress/wp-comments-post.php
#Installation de rsync pour déplacer les fichiers
[steve@web download]$ sudo yum install rsync -y
[...]
Installed:
rsync.x86_64 0:3.1.2-10.el7

Complete!
```
---
* Déplacement des fichiers dans un dossier servi par Apache : 
```bash
[steve@web download]$ sudo rsync -avP /home/steve/download/wordpress/ /var/www/html/
[...]
sent 52,081,192 bytes  received 42,909 bytes  104,248,202.00 bytes/sec
total size is 51,918,380  speedup is 1.00
```
* * Vérification du déplacement : 
```bash
[steve@web download]$ sudo ls -al /var/www/html/
[...]
drwxr-xr-x. 5 nobody 65534 4096 Dec  8 23:13 wordpress
```
---
* Ajout d'un fichier pour les dl de Wordpress : 
```bash
[steve@web download]$ sudo mkdir /var/www/html/wp-content/uploads
```
---
* Ajout des droits sur les dossiers/fichiers de /html pour l'utilisateur apache : 
```bash
[steve@web download]$ sudo chown -R apache:apache /var/www/html/*
```
* * Vérification des droits : 
```bash
[steve@web download]$ sudo ls -al /var/www/html/*
-rw-r--r--.  1 apache apache   405 Feb  6  2020 /var/www/html/index.php
[...]
-rw-r--r--.  1 apache apache    647 Feb  6  2020 wp-diff.php
```
---
* Renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données : 

* * création du fichier de config à partir de wp-config-sample.php : 
```bash
[steve@web /]$ cd /var/www/html
[steve@web html]$ sudo cp wp-config-sample.php wp-config.php
```

* * Modification du fichier de config wp-config.php : 
```bash
[steve@web html]$ sudo vim wp-config.php
[...]
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_web' );

/** MySQL database username */
define( 'DB_USER', 'dbweb' );

/** MySQL database password */
define( 'DB_PASSWORD', 'OKijuh' );

/** MySQL hostname */
define( 'DB_HOST', '10.99.99.12' );
[...]
```
---
**Vérification :**


Le site est accessible depuis le navidateur WEB.
L'installation s'est terminé via l'interface web.

# III. Reverse Proxy

**Énoncé :**

Installer un serveur NGINX (paquet `epel-release` puis `nginx` sur CentOS).

Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80. 

Configurer le firewall de NGINX afin d'accepter ce trafic. 

Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy. 

**Solution :**

* Installer un serveur NGINX : 
```bash
[steve@rp ~]$ sudo yum insta epel-release -y
Installed:
epel-release.noarch 0:7-11

Complete!
```
```bash
[steve@rp ~]$ sudo yum install nginx -y
Installed:
  nginx.x86_64 1:1.16.1-3.el7

Dependency Installed:
  centos-indexhtml.noarch 0:7-9.el7.centos          dejavu-fonts-common.noarch 0:2.33-6.el7
[...]
  nginx-mod-mail.x86_64 1:1.16.1-3.el7              nginx-mod-stream.x86_64 1:1.16.1-3.el7
  openssl11-libs.x86_64 1:1.1.1g-2.el7

Complete!
```

* Configurer NGINX pour qu'il puisse renvoyer vers le serveur web :
```bash
[steve@rp ~]$ sudo vim /etc/nginx/nginx.conf

	events {}
	
	http {
		server {
			listen       80;
	
			server_name web.cesi;
	
			location / {
				proxy_pass      http://10.99.99.11:80;
			}
		}
	
[steve@rp ~]$ sudo systemctl start nginx
[steve@rp ~]$ sudo systemctl enable nginx
[steve@rp ~]$ sudo systemctl status nginx
[...]
   Active: active (running) since Wed 2021-01-06 21:16:10 CET; 57s ago
[...]
```
* Ajout de l'adresse IP du serveur rp au fichier Hosts client : 
```bash
C:\WINDOWS\System32\drivers\etc\hosts
    10.99.99.13 web.cesi
```

---
**Vérification :**

```bash
[steve@db ~]$ sudo vim /etc/hosts
	[...]
	10.99.99.13     web.cesi
	~
	~
[steve@db ~]$ curl web.cesi
<!doctype html>
<html lang="fr-FR" >
<head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>WebLinux &#8211; Un site utilisant WordPress</title>

[...]
   /(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
        </script>

</body>
</html>
```

http://web.cesi fonctionnne dans le navigateur web du client

# IV. Un peu de sécu

## 1. fail2ban

**Énoncé :**

fail2ban est un outil très commun au sein des systèmes GNU/Linux, il permet de limiter les attaques à base de flood/spam (comme attaque par bruteforce).

Son fonctionnement est très simple :
* il lit en temps réel des fichiers de logs
* il repère dans ces fichiers de logs des éléments importants (expression régulière)
* il effectue des actions en fonction de ce qu'il a vu dans les logs

Cas que vous devez mettre en place :
* il lit en temps réel le fichier de log du démon SSH `sshd`
* `sshd` écrit chaque tentative de connexion dans le fichier
* si un certain nombre d'échecs de connexions sont réalisés dans un certain laps de temps, fail2ban devra le détecter
* fail2ban utilisera alors le pare-feu pour bloquer l'IP qui a essayé de se connecter

Là encore, inspirez-vous de ce que vous pourrez trouver sur Internet, c'est un cas d'école très classique.

---
**Solution :**

> Ce qui suit a été fait sur chaque VM (web / db / rp)

* Installation du paquet `epel-release` nécessaire à l'installation du package `fail2ban` :

```bash
[steve@db ~]$ sudo yum install epel-release -y
[...]
Installed:
  epel-release.noarch 0:7-11

Complete!
```
 * Installation du package `fail2ban` :
 
```bash
[steve@db ~]$ sudo yum install fail2ban -y
[...]
Installed:
  fail2ban.noarch 0:0.11.1-10.el7

Dependency Installed:
  fail2ban-firewalld.noarch 0:0.11.1-10.el7   fail2ban-sendmail.noarch 0:0.11.1-10.el7   fail2ban-server.noarch 0:0.11.1-10.el7
  systemd-python.x86_64 0:219-78.el7_9.2

Complete!
```

* Démarrage du service *fail2ban* :

```bash
[steve@db ~]$ sudo systemctl start fail2ban
[steve@db ~]$ sudo systemctl enable fail2ban
Created symlink from /etc/systemd/system/multi-user.target.wants/fail2ban.service to /usr/lib/systemd/system/fail2ban.service.
```
* Création du fichier de conf `/etc/fail2ban/jail.local` :

```bash
[steve@db ~]$ sudo vim /etc/fail2ban/jail.local
	[DEFAULT]
	# Ban hosts for one hour:
	ignoreip = 127.0.0.1/8
	bantime = 180
	maxretry = 3
	
	# Override /etc/fail2ban/jail.d/00-firewalld.conf:
	banaction = iptables-multiport
	
	[sshd]
	enabled = true
```

*Redémmarage du service *fail2ban* :

```bash
[steve@db ~]$ sudo systemctl restart fail2ban
```
---
**Vérification :**

```bash
[steve@db ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd

[steve@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     0
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     0
   `- Banned IP list:
```

```bash
sudo tail -f /var/log/fail2ban.log
[...]
2021-01-08 10:51:58,154 fail2ban.filter         [2519]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 10:47:50
2021-01-08 10:51:58,155 fail2ban.filter         [2519]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 10:47:58
2021-01-08 10:53:41,584 fail2ban.filter         [2519]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 10:53:41
2021-01-08 10:53:42,294 fail2ban.actions        [2519]: NOTICE  [sshd] Ban 10.99.99.1
2021-01-08 11:02:16,021 fail2ban.actions        [2519]: NOTICE  [sshd] Unban 10.99.99.1
```

## 2. HTTPS

### A. Génération du certificat et de la clé privée associée

**Énoncé :**

> A faire sur la machine frontale Web : le reverse proxy.

Commande pour générer une clé et un certificat auto-signé :
```bash
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
```

**NB :** lorsque vous taperez la commande de génération de certificat, un prompt vous demandera des infos qui seront inscrites dans le certificat. Vous pouvez mettre ce que vous voulez **SAUF** pour l'étape où on vous demande :
```bash
Common Name (e.g. server FQDN or YOUR name) []:
```
**Il est impératif de saisir ici le FQDN ou hostname qui sera utilisé par les clients pour joindre le service.** Dans notre cas c'est `web.cesi`.

Le certificat et la clé ont été générés dans le répertoire courant. Vous pouvez les déplacez où vous le souhaitez ; il existe cependant un path réservé à ça dans CentOS :
* `/etc/pki/tls/certs/` pour les certificats
* `/etc/pki/tls/private/` pour les clés

---
**Solution :**

* Génération d'une clé et d'un certificat auto-signé :

```bash
[steve@rp ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
Generating a 2048 bit RSA private key
....................................................+++
..............................................................+++
writing new private key to 'web.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:France
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:web.cesi
Email Address []:
```

* Déblacement du certificat et de la clé :

```bash
[steve@rp ~]$ sudo mv web.cesi.crt /etc/pki/tls/certs/
[steve@rp ~]$ sudo mv web.cesi.key /etc/pki/tls/private/
```

### B. Configurer le reverse proxy

**Énoncé :**

> A faire sur la machine frontale Web : le reverse proxy.

Configurer le reverse proxy pour écouter sur le port 443 (TCP). Le port 443 doit mettre en place un chiffrement TLS avec la clé et le certificat qui viennent d'être générés. Il doit toujours rediriger vers l'application Web Wordpress.

Si le port 80 est atteint, le trafic doit être redirigé automatiquement vers le port 443 (ça se fait dans la configuration de NGINX).

> Gardez en tête la commande `sudo ss -alnpt` pour avoir une idée des ports TCP en écoute sur votre machine. Vous pouvez vous assurer que votre serveur tourne bien sur le port `80/tcp` et `443/tcp` de cette façon. Pour tester la redirection automatique de port 80 vers 443, il faudra vous connecter au site comme un client normal, avec un navigateur.

---
**Solution :**

* Modification du fichier de config NGINX `/etc/nginx/nginx.conf` :

```bash
[steve@rp ~]$ sudo vim /etc/nginx/nginx.conf

	events {}
	
	http {
		server {
			listen       80;
	
			server_name web.cesi;
			return 301 https://web.cesi$request_uri;
	
			location / {
			}
		}
	
		server {
			listen       443 ssl http2;
			server_name  web.cesi;
	
			ssl_certificate "/etc/pki/tls/certs/web.cesi.crt";
			ssl_certificate_key "/etc/pki/tls/private/web.cesi.key";
			ssl_session_cache shared:SSL:1m;
			ssl_session_timeout  10m;
			ssl_ciphers HIGH:!aNULL:!MD5;
			ssl_prefer_server_ciphers on;
	
			location / {
				proxy_pass   http://10.99.99.11:80;
			}
	
		}
	
	}
```

* Ouverture du port 443 :

```bash
[steve@rp ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[steve@rp ~]$ sudo firewall-cmd --reload
success
[steve@rp ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
---
**Vérification :**

```bash
[steve@rp ~]$ curl https://web.cesi -k
<!doctype html>
<html lang="fr-FR" >
<head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>WebLinux &#8211; Un site utilisant WordPress</title>
[...]
        /(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",(function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())}),!1);
        </script>

</body>
</html>
```

## 3. Monitoring

### A. Installation de Netdata

**Énoncé :**

Installer Netdata sur les 3 machines et tester qu'il fonctionne bien en accédant à l'interface Web. Il faudra ouvrir le port firewall `19999/tcp` pour accéder à l'interface de Netdata.

---

**Solution :**

>Ce qui suit à été fait sur les 3 VM

* Installation de Netdata :

```bash
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

```bash
System            : Linux
Operating System  : GNU/Linux
Machine           : x86_64
BASH major version:
 --- Fetching script to detect required packages... ---
[/tmp/netdata-kickstart-pjTMg0OIq3]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-pjTMg0OIq3/install-required-packages.sh https://raw.githubusercontent.com/netdata/netdata/master/packaging/installer/install-required-packages.sh  OK

 --- Running downloaded script to detect required packages... ---
[/tmp/netdata-kickstart-pjTMg0OIq3]$ sudo /usr/bin/bash /tmp/netdata-kickstart-pjTMg0OIq3/install-required-packages.sh netdata Loading /etc/os-release ...
You should have EPEL enabled to install all the prerequisites.
Check: http://www.tecmint.com/how-to-enable-epel-repository-for-rhel-centos-6-5/
 > CentOS Version: 7 ...
 > Checking for epel ...
[...]
Setting netdata.tarball.checksum to 'new_installation'

 --- We are done! ---

  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata                          .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed and running now!  -'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  enjoy real-time performance and health monitoring...
```

* Ouverture du port *19999* :

```bash
[steve@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[steve@web ~]$ sudo firewall-cmd --reload
success
[steve@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
---
**Vérifications :**

```bash
[steve@web ~]$ curl 10.99.99.11:19999
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" content="netdata"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=e
[...]
></ul></nav></div></nav><div class="navbar-highlight"><div id="navbar-highlight-content" class="navbar-highlight-content"></div></div><div id="masthead" style="display:none"><div class="container"><div class="row"><div class="col-md-7"><h1>Netdata<p class="lead">Real-time performance monitoring, in the greatest possible detail</p></h1></div><div class="col-md-5"><div class="well well-lg"><div class="row"><div class="col-md-6"><b>Drag</b> charts to pan. <b>Shift + wheel</b> on them, to zoom in and out. <b>Double-click</b> on them, to reset. <b>Hover</b> on them too!</div><div class="col-md-6"><div class="netdata-container" data
```

### B. Alerting

**Énoncé :**

Définir une configuration qui permet de recevoir une [alerte Discord](https://learn.netdata.cloud/docs/agent/health/notifications/discord) si le disque OU la RAM sont remplis à + de 75%.

---
**Solution :**

* Configuration du fichier pour les notif discord :
```bash
[steve@rp ~]$sudo /etc/netdata/edit-config health_alarm_notify.conf
	#------------------------------------------------------------------------------
	# discord (discordapp.com) global notification options
	
	# multiple recipients can be given like this:
	#                  "CHANNEL1 CHANNEL2 ..."
	
	# enable/disable sending discord notifications
	SEND_DISCORD="YES"
	
	# Create a webhook by following the official documentation -
	# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
	DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/797809642658988074/pThJry_4z9sHFlayFqJHZwzYeOdZcCxsuOl11OrGfUuxgCnIyn3soVB4KTWGImtCsLs4"
	
	# if a role's recipients are not configured, a notification will be send to
	# this discord channel (empty = do not send a notification for unconfigured
	# roles):
	DEFAULT_RECIPIENT_DISCORD="rp"

Editing '/etc/netdata/health_alarm_notify.conf' ...
 1250L, 46908C written
 1250L, 46908C written
```
---
**Vérifications :**

* Test des notifications :
```bash
[steve@rp ~]$ /usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-01-10 16:05:31: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.lab test.chart.test_alarm is WARNING to 'rp'
2021-01-10 16:05:31: alarm-notify.sh: INFO: sent email notification for: rp.tp2.lab test.chart.test_alarm is WARNING to 'root'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-01-10 16:05:31: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.lab test.chart.test_alarm is CRITICAL to 'rp'
2021-01-10 16:05:32: alarm-notify.sh: INFO: sent email notification for: rp.tp2.lab test.chart.test_alarm is CRITICAL to 'root'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-01-10 16:05:32: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.lab test.chart.test_alarm is CLEAR to 'rp'
2021-01-10 16:05:32: alarm-notify.sh: INFO: sent email notification for: rp.tp2.lab test.chart.test_alarm is CLEAR to 'root'
# OK
```